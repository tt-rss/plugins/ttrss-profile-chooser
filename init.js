/* global App, Plugins, xhr */

Plugins.Profile_Chooser = {
	onChange: function(sender) {
		if (confirm("Activate selected profile? Window will reload.")) {
			xhr.post("backend.php", {op: "pref-prefs", method: "activateprofile", id: sender.attr('value')}, () => {
				window.location.reload();
			});
		} else {
			// there has to be a better way to revert select to a previous value in onchange...

			xhr.json("backend.php", App.getPhArgs("profile_chooser", "getprofile"), (reply) => {
				if (reply && typeof reply.profile != 'undefined')
					sender.attr('value', reply.profile, false);
			});
		}
	}
};
