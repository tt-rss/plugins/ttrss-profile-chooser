<?php
class Profile_Chooser extends Plugin {

	function about() {
		return array(null,
			"Adds a profile chooser to the main toolbar",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_MAIN_TOOLBAR_BUTTON, $this);
	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function getprofile() : void {
		print json_encode(["profile" => $_SESSION["profile"] ?? 0]);
	}

	function hook_main_toolbar_button() {
		$profiles = ORM::for_table("ttrss_settings_profiles")
			->where("owner_uid", $_SESSION["uid"])
			->order_by_expr("title")
			->find_many();

		$dropdown_items = [ 0 => __("Default profile") ];

		foreach ($profiles as $profile) {
			$dropdown_items[$profile->id] = $profile->title;
		}

		echo \Controls\select_hash("profile_chooser", $_SESSION["profile"] ?? "",
			$dropdown_items, ["style" => "order: 21", "onchange" => "Plugins.Profile_Chooser.onChange(this)"]);
	}

	function api_version() {
		return 2;
	}

}
